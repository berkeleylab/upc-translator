#include <upc.h>
#include "stdlib.h"
#include "math.h"

#define NUM_FIBER 16
#define DIM 16


shared [NUM_FIBER] double fiber_x[THREADS][NUM_FIBER];
shared [NUM_FIBER] double fiber_y[THREADS][NUM_FIBER];

int sqrtProcs;
int my_dim;

/* for dynamic environment - compiler crashes here - need better error message */
shared [DIM*DIM / THREADS] double *fluid;

int main(int argc, char** argv) {
  
  int minX, minY, maxX, maxY;
  int i,j;
  double r; 
  int x,y;

  if (argc < 3) {
    printf("wrong number of arguments\n");
    upc_global_exit(1);
  }  
  minX = minY = atoi(argv[1]);
  maxX = maxY = atoi(argv[2]);
  
  sqrtProcs = (int) pow(THREADS, 0.5);
  my_dim = DIM / sqrtProcs;

 
  upc_forall(i = 0; i < THREADS; i++; i) {
    for (j = 0; j < NUM_FIBER; j++) {
      r = ((double) rand()) / RAND_MAX;
      fiber_x[i][j] = r;
       r = ((double) rand()) / RAND_MAX;
      fiber_y[i][j] = r;
    }
  }
  
  my_dim = DIM / sqrtProcs;
  
  
  fluid = upc_all_alloc(THREADS, DIM * DIM / THREADS);
  upc_forall(i = 0; i < DIM * DIM; i++; &fluid[i]) {
    fluid[i] = MYTHREAD;
  }

 
  upc_forall(i = 0; i < THREADS; i++; i) {
    for (j = 0; j < NUM_FIBER; j++) {
      x = fiber_x[i][j] / 2;
      y = fiber_y[i][j] / 2;
      fiber_x[i][j] = 8*fluid[translate(x,y)] - fluid[translate(x+1,y)] - fluid[translate(x-1,y)] - 
	fluid[translate(x,y+1)] - fluid[translate(x,y-1)] - fluid[translate(x+1,y+1)] - 
	fluid[translate(x+1,y-1)] - fluid[translate(x-1,y-1)] - fluid[translate(x-1,y+1)];
    }
  }
}   

  
  
