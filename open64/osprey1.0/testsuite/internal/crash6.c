#include <upc.h>

shared float *a;

int main() {
  int i, j, k ;

  upc_forall( j = 2; j < 345; j++; continue) 
    upc_forall(k=7; k <234; k++; &a[k]) {
    upc_forall(i = 0; i < 1000; i++; 333) {
          a[k] = a[k+1];
    } 
  }
}
