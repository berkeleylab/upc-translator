#include <upc.h>

shared void *p;
shared void *pp;
upc_lock_t *l;

int main() {
  int j;
  upc_barrier 4;
  upc_barrier;
  upc_wait;
  upc_wait 4;
  upc_notify;
  upc_notify 3;

  p = upc_global_alloc(4,4);
  p = upc_all_alloc(4,4);
  p = upc_local_alloc(4,4);
  upc_free(p);
  
  j = upc_phaseof(p);
  j = upc_threadof(p);
  j = upc_addrfield(p);

  l = upc_global_lock_alloc();
  l = upc_all_lock_alloc();
  upc_lock_init(l);
  upc_lock(l);
  j = upc_lock_attempt(l);
  upc_unlock(l);

  upc_memcpy(p,pp,34);
  upc_memget(&j,p,32);
  upc_memput(pp,&j,2);
  upc_memset(p, 3, 3);

  upc_fence;
  
  
}
