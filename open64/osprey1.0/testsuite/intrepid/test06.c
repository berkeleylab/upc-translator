
#include <upc_strict.h>
#include <stdio.h>

#define FACTOR 2
shared int array[FACTOR*THREADS];
shared int *pp;
int *s;
void
test06()
{
  int i;
  
  s = (int *)&array[1];
  /*
    for (i = MYTHREAD; i < FACTOR*THREADS; i += THREADS)
    {
    
    s = (int *)&array[i];
    *s = i+1;
    }
    upc_barrier;
  */
  /*
  if (MYTHREAD == 0)
    {
      for (i = 0; i < FACTOR*THREADS; ++i)
	{
    shared int *got;
    int expected = i+1;
    got = (shared int *)&array[i];
    if (*got != expected) {
      
      fprintf(stderr,
    "test06: error at element %d. Expected %d, got %d\n",
    i, expected, *got);
    abort ();
    }
    }
    printf ("test06: test shared->local, and shared pointers - passed.\n");
    }
  */
}

main()
{
  test06 ();
  exit (0);
}
